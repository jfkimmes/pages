<!DOCTYPE html>

<html lang="en">
<head>
<title>Perfect Firefox Trackpad Sensitivity on Wayland | jfkimmes.eu</title>
<meta charset="utf-8"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<meta content="Pelican" name="generator">
<link href="https://jfkimmes.eu/feeds/all.atom.xml" rel="alternate" title="jfkimmes.eu Full Atom Feed" type="application/atom+xml"/>
<link as="font" crossorigin="" href="https://jfkimmes.eu/theme/fonts/Inter-Regular.woff2?v=3.18" rel="preload" type="font/woff2"/>
<link as="font" crossorigin="" href="https://jfkimmes.eu/theme/fonts/FiraCode-Regular.woff2" rel="preload" type="font/woff2"/>
<link href="https://jfkimmes.eu/theme/css/style.css" rel="stylesheet" type="text/css">
<link href="https://social.tinycyber.space/@jfkimmes" rel="me">
</link></link></meta></head>
<body class="home" id="index">
<!-- Chrome Warning -->
<div id="chrome-warning">
<strong>Oh no! This site works best on Firefox. It looks like you are using a Chromium-based browser (Chrome, Edge, Opera, etc.) 😭 </strong>
<br/>
            To help save the open web <em>and</em> protect your privacy, you should give <a href="https://www.mozilla.org/en-US/firefox/new/">Firefox</a> a try!
        </div>
<script type="text/javascript">
            var isChrome = !!window.chrome;
            if (isChrome) {
                let warning = document.getElementById("chrome-warning");    
                warning.style.display = "block";
            }
        </script>
<!-- /Chrome Warning -->
<div id="header-bar">
<!-- BANNER -->
<header class="body" id="banner">
<h1><a href="https://jfkimmes.eu/">[jfkimmes.eu ~]# <span class="blinking-cursor">_</span> </a></h1>
</header>
<!-- /#banner -->
<!-- MENU -->
<nav id="menu"><ul>
<li><a href="/about/" title="About Me"><img src="https://jfkimmes.eu/theme/images/info.svg"/></a></li>
<li><a href="https://codeberg.org/jfkimmes" rel="noopener noreferrer" target="_blank" title="Codeberg Profile"><img src="https://jfkimmes.eu/theme/images/code.svg"/></a></li>
<li><a href="https://app.hackthebox.eu/profile/85651" rel="noopener noreferrer" target="_blank" title="HackTheBox Profile"><img src="https://jfkimmes.eu/theme/images/box.svg"/></a></li>
<li><a href="https://social.tinycyber.space/@jfkimmes" rel="noopener noreferrer" target="_blank" title="Mastodon Profile"><img src="https://jfkimmes.eu/theme/images/mastodon.svg"/></a></li>
<li><a href="https://matrix.to/#/@jfkimmes:hackingfor.eu" rel="noopener noreferrer" target="_blank" title="Direct Message"><img src="https://jfkimmes.eu/theme/images/message-circle.svg"/></a></li>
<li><a href="mailto:hello@jfkimmes.eu" rel="noopener noreferrer" target="_blank" title="E-Mail"><img src="https://jfkimmes.eu/theme/images/mail.svg"/></a></li>
<li><a href="/feeds/all.atom.xml" rel="noopener noreferrer" target="_blank" title="RSS Feed"><img src="https://jfkimmes.eu/theme/images/rss.svg"/></a></li>
</ul></nav>
<!-- /#menu -->
</div>
<!-- CONTENT -->
<section class="body" id="content">
<header>
<h1 class="entry-title">
<a href="https://jfkimmes.eu/posts/firefox-trackpad-on-wayland/" rel="bookmark" title="Permalink to Perfect Firefox Trackpad Sensitivity on Wayland">Perfect Firefox Trackpad Sensitivity on Wayland</a></h1>
</header>
<footer class="post-info">
<address class="vcard author">
<a class="h-card" href="https://jfkimmes.eu/about/">J. Florian Kimmes</a>
</address>
    //
    <time class="published" datetime="2024-02-22T00:00:00+01:00">
        2024-02-22
    </time>
</footer><!-- /.post-info -->
<div class="entry-content">
<p><em>TL;DR at the bottom.</em></p>
<p>The general trackpad situation on a modern laptop with GNOME on Wayland is <a href="https://www.youtube.com/watch?v=GBVeCADVe3M">fantastic</a>, these days! The only things I still tweak, are <a href="https://gitlab.com/warningnonpotablewater/libinput-config">scroll sensitivity in <code>libinput</code></a> for GTK3 applications, and for Firefox. Both for the same reason: they scroll way too fast. I want the good ol' page-sticks-to-my-fingers scroll experience. </p>
<p>When I set up my new work laptop recently, I realized two things.
Firstly, that my go-to trackpad settings were still not turned on by default in Firefox (as of Firefox 124.0), and secondly, that these settings are pretty hard to find if you don't know what you're looking for.</p>
<h2>The 'Pixel-based' Delta Mode</h2>
<p>Mozilla added a new 'pixel-based' scrolling mode to Firefox, two years ago, in response to users' complaints about too fast scrolling on Wayland. The pixel-based <code>kinetic_scroll.delta_mode</code> will calculate deltas on a per-pixel basis, allowing for more granular control of the scrolling speed.</p>
<p>To turn it on, switch this <code>delta_mode</code> to <code>2</code> for both <code>kinetic_scroll</code> and <code>pandgesture</code> settings. (Copy the following to your user.js or search for the settings in <code>about:config</code>)</p>
<div class="highlight"><pre><span></span><code>user_pref("apz.gtk.kinetic_scroll.delta_mode", 2);
user_pref("apz.gtk.pangesture.delta_mode", 2);
</code></pre></div>
<p>There's a modifier for both deltas, called <code>pixel_delta_mode_multiplier</code> that sets the pixels per movement, effectively allowing for granular control of the sensitivity of each 'swipe'. If you like the default speed with this new mode turned on, you don't need the modifier. For me, this was still too fast, so I reduced both scrolling and panning to <code>20</code>.</p>
<div class="highlight"><pre><span></span><code>user_pref("apz.gtk.kinetic_scroll.pixel_delta_mode_multiplier", "20");
user_pref("apz.gtk.pangesture.pixel_delta_mode_multiplier", "20");
</code></pre></div>
<h2>Asynchronous Panning and Zooming (APZ)</h2>
<p>The settings under the <a href="https://wiki.mozilla.org/Platform/GFX/APZ"><code>apz</code></a> prefix looked all pretty interesting, so I started experimenting with other settings. Another one I can recommend is:</p>
<div class="highlight"><pre><span></span><code>user_pref("apz.overscroll.enabled", true);
</code></pre></div>
<p>This will add an overscroll animation on the top and bottom of the page, giving a nice visual feedback when you reached the end of a page.</p>
<h2>TL;DR</h2>
<p>If Firefox scrolls too fast for your taste on Wayland, add these to your user.js or change them in <code>about:config</code>.</p>
<div class="highlight"><pre><span></span><code>user_pref("apz.gtk.kinetic_scroll.delta_mode", 2);
user_pref("apz.gtk.pangesture.delta_mode", 2);
user_pref("apz.gtk.kinetic_scroll.pixel_delta_mode_multiplier", "20");
user_pref("apz.gtk.pangesture.pixel_delta_mode_multiplier", "20");
user_pref("apz.overscroll.enabled", true);
</code></pre></div>
</div><!-- /.entry-content -->
</section>
<div id="contact-me">
<span>Do you have any questions or thoughts on this post? Send me a <a href="https://jfkimmes.eu/about/">message</a>!</span>
</div>
<!-- /#content -->
<!-- CONTENT_INFO -->
<footer class="body" id="contentinfo">
<p align="center">Built with <a href="https://getpelican.com/">Pelican</a>, patched-up CSS, and <img alt="Club Mate Bottle" height="30px" src="/images/mate_tiny.png" style="padding: -10px"/>.</p>
</footer>
<!-- /#contentinfo -->
</body>
</html>
