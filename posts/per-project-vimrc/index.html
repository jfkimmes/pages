<!DOCTYPE html>

<html lang="en">
<head>
<title>Secure Project-specific (Neo)vim Config | jfkimmes.eu</title>
<meta charset="utf-8"/>
<meta content="width=device-width,initial-scale=1" name="viewport"/>
<meta content="Pelican" name="generator">
<link href="https://jfkimmes.eu/feeds/all.atom.xml" rel="alternate" title="jfkimmes.eu Full Atom Feed" type="application/atom+xml"/>
<link as="font" crossorigin="" href="https://jfkimmes.eu/theme/fonts/Inter-Regular.woff2?v=3.18" rel="preload" type="font/woff2"/>
<link as="font" crossorigin="" href="https://jfkimmes.eu/theme/fonts/FiraCode-Regular.woff2" rel="preload" type="font/woff2"/>
<link href="https://jfkimmes.eu/theme/css/style.css" rel="stylesheet" type="text/css">
<link href="https://social.tinycyber.space/@jfkimmes" rel="me">
</link></link></meta></head>
<body class="home" id="index">
<!-- Chrome Warning -->
<div id="chrome-warning">
<strong>Oh no! This site works best on Firefox. It looks like you are using a Chromium-based browser (Chrome, Edge, Opera, etc.) 😭 </strong>
<br/>
            To help save the open web <em>and</em> protect your privacy, you should give <a href="https://www.mozilla.org/en-US/firefox/new/">Firefox</a> a try!
        </div>
<script type="text/javascript">
            var isChrome = !!window.chrome;
            if (isChrome) {
                let warning = document.getElementById("chrome-warning");    
                warning.style.display = "block";
            }
        </script>
<!-- /Chrome Warning -->
<div id="header-bar">
<!-- BANNER -->
<header class="body" id="banner">
<h1><a href="https://jfkimmes.eu/">[jfkimmes.eu ~]# <span class="blinking-cursor">_</span> </a></h1>
</header>
<!-- /#banner -->
<!-- MENU -->
<nav id="menu"><ul>
<li><a href="/about/" title="About Me"><img src="https://jfkimmes.eu/theme/images/info.svg"/></a></li>
<li><a href="https://codeberg.org/jfkimmes" rel="noopener noreferrer" target="_blank" title="Codeberg Profile"><img src="https://jfkimmes.eu/theme/images/code.svg"/></a></li>
<li><a href="https://app.hackthebox.eu/profile/85651" rel="noopener noreferrer" target="_blank" title="HackTheBox Profile"><img src="https://jfkimmes.eu/theme/images/box.svg"/></a></li>
<li><a href="https://social.tinycyber.space/@jfkimmes" rel="noopener noreferrer" target="_blank" title="Mastodon Profile"><img src="https://jfkimmes.eu/theme/images/mastodon.svg"/></a></li>
<li><a href="https://matrix.to/#/@jfkimmes:hackingfor.eu" rel="noopener noreferrer" target="_blank" title="Direct Message"><img src="https://jfkimmes.eu/theme/images/message-circle.svg"/></a></li>
<li><a href="mailto:hello@jfkimmes.eu" rel="noopener noreferrer" target="_blank" title="E-Mail"><img src="https://jfkimmes.eu/theme/images/mail.svg"/></a></li>
<li><a href="/feeds/all.atom.xml" rel="noopener noreferrer" target="_blank" title="RSS Feed"><img src="https://jfkimmes.eu/theme/images/rss.svg"/></a></li>
</ul></nav>
<!-- /#menu -->
</div>
<!-- CONTENT -->
<section class="body" id="content">
<header>
<h1 class="entry-title">
<a href="https://jfkimmes.eu/posts/per-project-vimrc/" rel="bookmark" title="Permalink to Secure Project-specific (Neo)vim Config">Secure Project-specific (Neo)vim Config</a></h1>
</header>
<footer class="post-info">
<address class="vcard author">
<a class="h-card" href="https://jfkimmes.eu/about/">J. Florian Kimmes</a>
</address>
    //
    <time class="published" datetime="2023-02-01T00:00:00+01:00">
        2023-02-01
    </time>
</footer><!-- /.post-info -->
<div class="entry-content">
<hr/>
<h2><strong><em>Update</em></strong>: <code>exrc</code> is now secure in neovim</h2>
<p><a href="https://github.com/neovim/neovim/pull/20956">This PR</a> adds a prompt to neovim 0.9 before loading a local <code>.nvimrc</code> file, when <code>exrc</code> is enabled.</p>
<p>If you are using regular vim or stuck on an older neovim version, this post might still be interesting for you. The rest of this post is unchanged.</p>
<hr/>
<h2>Why do you want per-project vim configs?</h2>
<p>Usually, I have all my vim configurations stored in my dotfiles. These global configs are easy, as they don't change too often, and are applicable for all my personal projects. So when they <em>do</em> change, I do a git pull on all my devices and be done.  </p>
<p>However, I don't only contribute to my <em>own</em> projects but also to <em>other peoples'</em> projects. So from time to time it happens, unfortunately, that some projects' style guides disagree with <a href="https://www.youtube.com/watch?v=SsoOG6ZeyUI">certain things</a> in my config.</p>
<p>Up to now, I used to have <code>set exrc</code> in my <code>init.vim</code> file, for these cases. This setting enables reading of local <code>.nvimrc</code> config files in the current directory. The major downside of this solution is that it reads and executes everything from (1) a hidden file (2) on editor startup. That is a nasty combination that only waits for a desaster to happen. If I carelessly clone a malicious repository and open vim in one of its directories, it is already too late.</p>
<h2>Why is <code>set secure</code> not enough?</h2>
<p>There are a lot of stackoverflow answers and blog posts recommending <code>set exrc</code> as a solution to the per-project configuration. Usually they recommend to set the 'secure' setting additionally, to make it a safe solution. Unfortunately, this is not sufficient. The neovim help has the following to say:</p>
<p><a href="https://jfkimmes.eu/images/1675616382.png"><img alt="Image of vim's :help secure output in a terminal window." class="image-process-article" sizes="(min-width: 800px) 718px, (min-width: 600px) 530px, (min-width: 400px) 330px, (min-width: 350px) 300px, 100vw" src="https://jfkimmes.eu/images/derivatives/article/800w/1675616382.png" srcset="https://jfkimmes.eu/images/derivatives/article/300w/1675616382.png 300w, https://jfkimmes.eu/images/derivatives/article/600w/1675616382.png 600w, https://jfkimmes.eu/images/derivatives/article/800w/1675616382.png 800w, https://jfkimmes.eu/images/derivatives/article/1600w/1675616382.png 1600w"/></a></p>
<p>Note that there is a pretty strong restriction on the usefulness of this setting hidden in this sentence. Specifically:</p>
<blockquote>
<p>On Unix this option is only used if the ".nvimrc" or ".exrc" is not owned by you.</p>
</blockquote>
<p>This does not cover our threat model of a malicious git repository, at all! When cloning a repository, we own all files in there, of course, so this setting does absolutely nothing.</p>
<p>Neovim has decided to <a href="https://github.com/neovim/neovim/issues/5784#issuecomment-506993689">deprecate</a> the exrc option. So for neovim users the exrc setting will not even work anymore in the future.</p>
<h2>Solution</h2>
<p>Since I use <a href="https://direnv.net/">direnv</a> on many projects anyways, I decided to make it manage my per-project vim configs, too. This is convenient, because direnv needs to solve the security implications of loading random files into the shell anyways, so it should solve the 'malicious git repo' use case automatically. And to nobody's surprise, it does: </p>
<blockquote>
<p>In some target folder, create an .envrc file and add some export(1) and unset(1) directives in it.<br/>
On the next prompt you will notice that direnv complains about the .envrc being blocked. This is the security mechanism to avoid loading new files automatically. Otherwise any git repo that you pull, or tar archive that you unpack, would be able to wipe your hard drive once you cd into it.<br/>
So here we are pretty sure that it won’t do anything bad. Type direnv allow . and watch direnv loading your new environment</p>
</blockquote>
<p>So to make vim load my project's <code>.init.vim</code> file, I simply added the following <code>.envrc</code> to my projects:</p>
<div class="highlight"><pre><span></span><code><span class="k">export</span><span class="w"> </span><span class="n">VIMINIT</span><span class="o">=</span><span class="s1">'source $MYVIMRC'</span>
<span class="k">export</span><span class="w"> </span><span class="n">MYVIMRC</span><span class="o">=$</span><span class="n">PWD</span><span class="o">/.</span><span class="n">init</span><span class="o">.</span><span class="n">vim</span>
</code></pre></div>
<p>This loads the custom <code>.init.vim</code> whenever I open vim in the project's directory. However, it will <em>only</em> be loaded if I specifically ran <code>direnv allow .</code> in that project. This is a sufficient safeguard against malicious repos, since it is not fully automatic. I can decide if I want to load other people's <code>.envrc</code> files at all, and if I do, I carefully review all <code>.env</code>/<code>.envrc</code> files in the project.<br/>
After allowing it once, direnv will load the variables automatically, until the <code>.envrc</code> file changes or is specifically disallowed.</p>
<p>To extend my global <code>init.vim</code>, I source it and overwrite only the project-specific settings:</p>
<div class="highlight"><pre><span></span><code><span class="n">so</span> <span class="o">~/</span><span class="n">dotfiles</span><span class="o">/</span><span class="n">init</span><span class="p">.</span><span class="n">vim</span>
<span class="n">set</span> <span class="n">tabstop</span><span class="o">=</span><span class="mi">2</span>
<span class="n">set</span> <span class="n">shiftwidth</span><span class="o">=</span><span class="mi">2</span>
<span class="n">set</span> <span class="n">expandtab</span>
</code></pre></div>
<p>I hope this helps. If someone has an easier method, please let me know.</p>
</div><!-- /.entry-content -->
</section>
<div id="contact-me">
<span>Do you have any questions or thoughts on this post? Send me a <a href="https://jfkimmes.eu/about/">message</a>!</span>
</div>
<!-- /#content -->
<!-- CONTENT_INFO -->
<footer class="body" id="contentinfo">
<p align="center">Built with <a href="https://getpelican.com/">Pelican</a>, patched-up CSS, and <img alt="Club Mate Bottle" height="30px" src="/images/mate_tiny.png" style="padding: -10px"/>.</p>
</footer>
<!-- /#contentinfo -->
</body>
</html>
